from django.contrib import admin
from .models import Customer

class CustomerAdmin(admin.ModelAdmin):
    list_display = ["first_name", "last_name", "mobile1", "email", "date_of_birth", "residence_phone"]
    search_fields = ["first_name", "last_name", "mobile1", "mobile2", "email", "residence_phone", 'office_phone']
    list_filter = ["sex", "title", "cCat"]


admin.site.register(Customer, CustomerAdmin)
