from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name="index"),
    url(r'^customers/$', views.CustomerIndex.as_view(), name="view-all-customers"),
    url(r'^customers/details/(?P<id>\d+)/$', views.CustomerView.as_view(), name="customer-view"),
    url(r'^customers/add-new/$', views.AddCustomer.as_view(), name="customer-addnew"),
    url(r'^logout/$', views.Logout, name="logout"),
]
