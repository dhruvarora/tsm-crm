# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-10 05:35
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2016, 12, 10, 5, 35, 5, 569000, tzinfo=utc))),
                ('cCat', models.CharField(choices=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D')], max_length=2)),
                ('title', models.CharField(choices=[('NA', 'NA'), ('Mr.', 'Mr.'), ('Ms.', 'Ms.'), ('Mrs.', 'Mrs.'), ('S.', 'S.'), ('Dr.', 'Dr.'), ('Prof.', 'Prof.')], max_length=10)),
                ('first_name', models.CharField(max_length=100)),
                ('middle_name', models.CharField(blank=True, max_length=100, null=True)),
                ('last_name', models.CharField(blank=True, max_length=100, null=True)),
                ('sex', models.CharField(choices=[('Male', 'Male'), ('Female', 'Female')], max_length=15)),
                ('rCat', models.CharField(choices=[('NA', 'NA'), ('H', 'H'), ('S', 'S'), ('C', 'C'), ('M', 'M')], max_length=20, verbose_name='Religion')),
                ('anniversary', models.DateField(blank=True, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('mobile1', models.BigIntegerField(blank=True, null=True)),
                ('mobile2', models.BigIntegerField(blank=True, null=True)),
                ('residence_phone', models.BigIntegerField(blank=True, null=True)),
                ('office_phone', models.BigIntegerField(blank=True, null=True)),
                ('date_of_birth', models.DateField(blank=True, null=True)),
                ('address1', models.CharField(blank=True, max_length=100, null=True)),
                ('address2', models.CharField(blank=True, max_length=100, null=True)),
                ('locality', models.CharField(blank=True, max_length=100, null=True)),
                ('city', models.CharField(blank=True, default='Mew Delhi', max_length=100, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
            ],
        ),
    ]
