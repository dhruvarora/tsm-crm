from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .models import Customer

def Logout(request):
    logout(request)
    return redirect("customer:index")

class IndexView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, "index.html", {"title":"Login to Optics CRM"})
        else:
            return render(request, "login.html", {"title":"Index"})

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request,user)
                return render(request, "index.html", {
                "title":"Index"
                })
            else:
                return render(request, "login.html", {
                "title":"Login"
                })
        else:
            return render(request, "login.html", {
            "title":"Login",
            })

class CustomerView(View):
    def get(self, request, id):
        customer = Customer.objects.get(pk=id)

        return render(request, "customer-view.html", {
        "title":"Viewing Customer",
        "customer":customer,
        })

    def post(self, request, id):
        customer = Customer.objects.get(pk=id)

        customer.title = request.POST.get("title")
        customer.first_name = request.POST.get("fname")
        customer.middle_name = request.POST.get("mname")
        customer.last_name = request.POST.get("lname")
        customer.sex = request.POST.get("sex")
        customer.anniversary = request.POST.get("anndate")
        customer.email = request.POST.get("email")
        customer.comments = request.POST.get("specialRemarks")
        customer.addline2 = request.POST.get("add2")
        customer.addline1 = request.POST.get("add1")
        customer.locality = request.POST.get("locality")
        if request.POST.get("pincode") == "":
            customer.pincode = None
        else:
            customer.pincode = int(request.POST.get("pincode"))
        if request.POST.get("mobile1")=="" or None:
            customer.mobile1 = None
        else:
            customer.mobile1 = int(request.POST.get("mobile1"))
        if request.POST.get("rphone")=="" or None:
            customer.rphone = None
        else:
            customer.rphone = int(request.POST.get("rphone"))
        if request.POST.get("mobile2")=="" or None:
            customer.mobile2 = None
        else:
            customer.mobile2 = int(request.POST.get("mobile2"))
        if request.POST.get("ophone")=="" or None:
            customer.ophone = None
        else:
            customer.ophone = int(request.POST.get("ophone"))
        if request.POST.get("anndate")=="":
            customer.anniversary = None

        else:
            customer.anniversary = datetime.datetime.strptime(request.POST.get("ann-date"), "%d/%m/%Y").strftime("%Y-%m-%d")
        if request.POST.get("bod")=="":
            customer.date_of_birth = None
        else:
            customer.date_of_birth = datetime.datetime.strptime(request.POST.get("bod"), "%d/%m/%Y").strftime("%Y-%m-%d")
        customer.save()

        return render(request, "customer-view.html", {
        "title":"Viewing Customer",
        "customer":customer,
        })


class CustomerIndex(View):
    def get(self, request):
        customers = Customer.objects.all()[:10]
        return render(request, "customer-index.html",{
        "title":"View all customers",
        "customers":customers,
        })

class AddCustomer(View):
    def get(self, request):

        return render(request, "customer-new.html", {
        "title":"Add New Customer",
        })

    def post(self, request):
        title = request.POST.get("title")
        if request.POST.get("fname") == "":
            fname = None
        else:
            fname = request.POST.get("fname")
        if request.POST.get("mname") == "":
            mname = None
        else:
            mname = request.POST.get("mname")
        lname = request.POST.get("lname")
        email = request.POST.get("email")
        sex = request.POST.get("sex")
        if request.POST.get("mobile1")=="" or None:
            mobile1 = None
        else:
            mobile1 = int(request.POST.get("mobile1"))
        if request.POST.get("rphone")=="" or None:
            rphone = None
        else:
            rphone = int(request.POST.get("rphone"))
        if request.POST.get("mobile2")=="" or None:
            mobile2 = None
        else:
            mobile2 = int(request.POST.get("mobile2"))
        if request.POST.get("ophone")=="" or None:
            ophone = None
        else:
            ophone = int(request.POST.get("ophone"))
        if request.POST.get("ann-date")=="":
            anndate = None
        else:
            anndate = datetime.datetime.strptime(request.POST.get("ann-date"), "%d/%m/%Y").strftime("%Y-%m-%d")
            print anndate
        if request.POST.get("bod")=="":
            bod = None
        else:
            bod = datetime.datetime.strptime(request.POST.get("bod"), "%d/%m/%Y").strftime("%Y-%m-%d")
            print bod
        add1 = request.POST.get("add1")
        add2 = request.POST.get("add2")
        locality = request.POST.get("locality")
        if request.POST.get("pincode")=="":
            pincode = None
        else:
            pincode = int(request.POST.get("pincode"))

        customer = Customer.objects.create(title=title, first_name=fname, middle_name=mname, last_name=lname, sex=sex, anniversary=anndate, email=email, mobile1=mobile1, mobile2=mobile2,
        residence_phone=rphone, office_phone=ophone, date_of_birth=bod, addline1=add1, addline2=add2, locality=locality, pincode=pincode)

        return render(request, "customer-view.html", {
        "title":"Viewing Customer", "customer":customer
        })
