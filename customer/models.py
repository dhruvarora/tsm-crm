from __future__ import unicode_literals

from django.utils import timezone
from django.db import models

now = timezone.now()

class Customer(models.Model):
    date_added = models.DateField(default=now)
    cCat = models.CharField(max_length=2, choices=(
        ("A", "A"),
        ("B", "B"),
        ("C", "C"),
        ("D", "D")
    ))
    title = models.CharField(max_length=10, choices=(
        ("NA", "NA"),
        ("Mr.", "Mr."),
        ("Ms.", "Ms."),
        ("Mrs.", "Mrs."),
        ("S.", "S."),
        ("Dr.", "Dr."),
        ("Prof.", "Prof."),
    ))
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    sex = models.CharField(max_length=15, choices=(
        ("Male", "Male"),
        ("Female", "Female"),
    ))
    rCat = models.CharField(max_length=20, choices=(
        ("NA", "NA"),
        ("H", "H"),
        ("S", "S"),
        ("C", "C"),
        ("M", "M"),
    ), verbose_name="Religion")
    anniversary = models.DateField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    mobile1 = models.BigIntegerField(blank=True, null=True)
    mobile2 = models.BigIntegerField(blank=True, null=True)
    residence_phone = models.BigIntegerField(blank=True, null=True)
    office_phone = models.BigIntegerField(blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    addline1 = models.CharField(max_length=100, blank=True, null=True, verbose_name="Address Line 1")
    addline2 = models.CharField(max_length=100, blank=True, null=True, verbose_name="Address Line 2")
    city = models.CharField(default="Delhi", max_length=100, blank=True, null=True)
    pincode = models.IntegerField(blank=True, null=True)
    locality = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True, default="India")

    def __str__(self):
        return self.first_name + " " + self.last_name
