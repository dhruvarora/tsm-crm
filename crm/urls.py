from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'', include("customer.urls", namespace="customer")),
    url(r'^orders/$', include("orders.urls", namespace="orders")),
    url(r'^admin/', admin.site.urls),
]
