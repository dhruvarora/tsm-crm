from django.shortcuts import render
from django.views.generic import View

from customer.models import Customer

class OrderIndex(View):

    def get(self, request):
        title = "Create Order"
        customer = Customer.objects.all()

        return render(request, "orders-new.html", {
        "customer":customer, "title":title,
        })
