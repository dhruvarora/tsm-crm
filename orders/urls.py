from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'', views.OrderIndex.as_view(), name="index")
]
