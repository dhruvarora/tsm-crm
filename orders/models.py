from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from customer.models import Customer

now = timezone.now()

class Order(models.Model):
    date_added = models.DateField(default=now)
    customer = models.ForeignKey(Customer)
    subtotal = models.FloatField()
    discount = models.FloatField(default=0)
    tax = models.FloatField(default=0)
    total = models.FloatField()
    status = models.CharField(max_length=150)

    def __str__(self):
        return self.pk

class OrderItems(models.Model):
    order = models.ForeignKey(Order)
    brand = models.CharField(max_length=20)
    color = models.CharField(max_length=20)
    laces = models.CharField(max_length=1)
    notes = models.CharField(max_length=150, blank=True, null=True)
    cost = models.FloatField()
